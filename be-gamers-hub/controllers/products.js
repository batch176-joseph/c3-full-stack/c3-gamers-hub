//***CONTROLLER - PRODUCT***
//Dependencies and Modules
	const Product = require('../models/Product');
	const User = require('../models/User');

//Controller - Add New Product
	module.exports.addProduct = (reqBody) => {
		let newCourse = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		});
		return newCourse.save().then((course, err ) => {
			if (err) {
				return false;
			} else {
				return true;
			};
		}).catch(err => err)
	};

	//Get all products
	module.exports.getAllProducts = () => {
		return Product.find({}).then(result => {
			return result;
		});
	};

	//Get all ACTIVE products
	module.exports.getAllActive = () => {
		return Product.find({isActive: true}).then(result => {
			return result;
		}).catch(err => err)
	};

	//Get SPECIFIC products
	module.exports.getProduct = (reqParams) => {
		return Product.findById(reqParams).then(result => {
			return result;
		}).catch(err => err)
	};

//Controller - Update A Product
	module.exports.updateProduct = (productId, data) => {
		let updatedProduct = {
			name: data.name,
			description: data.description,
			price: data.price
		}
		return Product.findByIdAndUpdate(productId, updatedProduct).then((product, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		}).catch(error => error)
	};

//Controller - Archive A Product
	module.exports.archiveProduct = (productId) => {
		let updateActiveField = {
			isActive: false
		};

		return Product.findByIdAndUpdate(productId, updateActiveField).then((course, error) => {

			if(error){
				return false;
			} else {
				return true;
			}
		}).catch(error => error)
	}

//Controller - Activating A Product
	module.exports.activateProduct = (productId) => {
		let updateActiveField = {
			isActive: true
		};

		return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {

			if(error){
				return false;
			} else {
				return true;
			}
		}).catch(error => error)
	}
