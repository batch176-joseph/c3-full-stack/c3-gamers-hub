//***CONTROLLER - ORDER***
//Dependencies and Modules
	const Order = require('../models/Order');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth.js')
	const Product = require('../models/Product');
	const User = require('../models/User');

//Controller - Order Product
	module.exports.order = async (req, res) => {
		if (req.user.isAdmin) {
			return res.send({ message: "Action Forbidden" })
		}

		let isUserUpdated = await User.findById(req.user.id).then( user => {

			let newOrder = {
				productId: req.body.productId
			}

			user.orders.push(newOrder);

			//save the changes made to our user document
			return user.save().then(user => true).catch(err => err.message)

			//if isUserUpdated does not contain the boolean true, we will stop our process and return a message to our client
			if (isUserUpdated !== true) {
				return res.send ({ message: isUserUpdated })
			}
		});

		let isProductUpdated = await Product.findById(req.body.productId).then(product => {
			let user = {
				userId: req.user.id
			}

			product.orders.push(user);

			return product.save().then(product => true).catch(err => err.message)

			if (isProductUpdated !== true) {
				return res.send({ message: isProductUpdated })
			}
		})

		if (isUserUpdated && isProductUpdated) {
			return res.send({ message: "Ordered Successfully" })
		}

	}

//Get User Order
  	module.exports.getUserOrders = (data, userID) => {
	    return User.findById(userID,{name:0,email:0,password:0,isAdmin:0}).then(result => {
	      return result;
	    })
	  }

//Get All Order
  	module.exports.getAllOrders = () => {
		return User.find({},{name:0,email:0,password:0,isAdmin:0}).then(result => {
	    	return result
	  	}).catch(error => error);
	}