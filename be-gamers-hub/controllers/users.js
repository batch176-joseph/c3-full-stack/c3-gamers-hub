//***CONTROLLER - USER***
//Dependencies and Modules
	const User = require('../models/User');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth.js')
	const Product = require('../models/Product');

//Environment Setup
	dotenv.config();
	const salt = process.env.SALT;

//Controller - Register User
	module.exports.register = (userInput) => {
		let email = userInput.email;
		let password = userInput.password;

		let newUser = new User({
			email: email,
			password: bcrypt.hashSync(password, parseInt(salt))
		});
		return newUser.save().then((user, err) => {
			if (user) {
				return user;
			} else {
				return err;
			};
		});
	};

//Controller - User Authentication
	module.exports.loginUser = (data) => {
		return User.findOne({email:data.email}).then(result => {
			if (result === null) {
				return false;
			} else {
				const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)
				if (isPasswordCorrect) {
					return {accessToken: auth.createAccessToken(result.toObject())}
				} else {
					return false;
				};
			};
		});
	};

//Controller - Get User Details
	module.exports.getProfile = data => {
		return User.findById(data).then(result => {
			result.password = '';
			return result;
		});
	};

//Controller - Get All User
	module.exports.getAllUser = (data) => {
		console.log('allusercontroller')
		return User.find().then(result => {
			result.password = '#';
			return result;
		})
	}

//Controller - Promote To Admin
	module.exports.enableAdmin = (userId) => {
		let updateAdminStatus = {
			isAdmin: true
		};
		return User.findByIdAndUpdate(userId, updateActiveStatus).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error);
	};

//Controller - Demote To User
	module.exports.disableAdmin = (userId) => {
		let updateAdminStatus = {
			isAdmin: false
		};
		return User.findByIdAndUpdate(userId, updateActiveStatus).then((product, error) => {
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error);
	};

//Controller - Create Order
	/*module.exports.order = async (req,res) => {
		if(req.user.isAdmin) {
			return res.send({ message: "Action Forbidden"})
		}
		let isUserUpdated = await User.findById(req.user.id).then(user => {
			let newOrder = {
					productName: req.body.productName,
					productQuantity: req.body.productQuantity,
					price: req.body.price,
					totalAmount: req.body.price*req.body.productQuantity
			};
			user.userOrders.push(newOrder);
			return user.save().then(user => true).catch(err => err.message);

			if (isUserUpdated !== true) {
				return res.send({message: isUserUpdated})
			}
		})
	//find productId
		let isProductUpdated = await Product.findById(req.body.productId).then(product => {
			let prodOrder = {
				orderId: req.user.id
			}
			product.productOrders.push(prodOrder);
			return product.save().then(product => true).catch(err => err.message)
			if (isProductUpdated !== true) {
				return res.send({message: isProductUpdated})
			}
		})
		if (isUserUpdated && isProductUpdated) {
			return res.send({message: "Order successfully submitted."})
		}
	};*/

//GET all history (admin)
	module.exports.allHistory = (req, res) => {
			User.findById(req.user.id) 
			.then(result => res.send(result.orders))
			.catch(err => res.send(err))
	} 

//##
//Get all history (user)
	module.exports.history = (req,res) => {
	User.findByUserId(req.params.userid, {orders:0}).then(result => {
		return result
	}).catch(error => error);
}



//##
//Get all orders (admin)
	module.exports.getAllOrders = () => {
	return User.find({},{email:0,password:0,isAdmin:0,shippingAddress:0,paymentMethod:0,cardNumber:0,isNewUser:0,dateRegisteredOn:0}).then(result => {
		return result
	}).catch(error => error);
}


//##
//Cancel This Order
	module.exports.cancelOrder = (orderId) => {
		console.log(orderId,'1');
		if(req.user.isAdmin) {
			return res.send({ message: "Action Forbidden"})
		}
		console.log(orderId,'2');
		return user.userOrders.findByIdAndDelete(orderId).then((order, error) => {
			console.log(orderId,'3');
			if (error) {
				return false;
			} else {
				return true;
			}
		}).catch(error => error);
	};
