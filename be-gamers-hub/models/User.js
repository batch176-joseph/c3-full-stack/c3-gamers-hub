//Models and Dependencies
	const mongoose = require('mongoose');

//Schema/Blueprint
	const userSchema = new mongoose.Schema({
		name: {
			type: String
		},
		email: {
			type: String,
			required: [true, 'Email is required'],
			unique: true
		},
		password: {
			type: String,
			required: [true, 'Email is required']
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		orders: [
			{
				productId: {
					type: String,
					required: [true, 'Product ID is required']
				},
				orderedOn: {
					type: Date,
					default: new Date()
				},
				status: {
					type: String,
					default: 'Ordered'
				}
			}
		]
	});

//Model
	module.exports = mongoose.model('User', userSchema);
