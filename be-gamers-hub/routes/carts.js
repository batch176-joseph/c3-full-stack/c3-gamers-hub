//***ROUTES - CART***
//Dependencies and Modules
  const Cart = require("../models/Cart");
  const express = require('express');
  const auth = require('../auth');

//Routing Component
  const route = express.Router();

//Verify
  const { verify, verifyAdmin } = auth;

//CREATE

route.post("/addToCart", verify, async (req, res) => {
  
})

module.exports = route;