//***ROUTES - USER***
//Dependencies and Modules
	const express = require('express');
	const controllers = require('../controllers/users');
	const auth = require('../auth');

//Routing Component
	const route = express.Router();

//Verify
	const { verify, verifyAdmin } = auth;

//Routes - Register User
	route.post('/register', (req, res) => {
		/*console.log(req.body);*/
		let userInput = req.body;
		controllers.register(userInput).then(outcome => { res.send(outcome);
		});
	});

//Routes - User Authentication
	route.post('/login', (req, res) => {
		controllers.loginUser(req.body).then(result => res.send(result));
	});

//Routes - Get User Details
	route.get('/details', auth.verify, (req,res) => {
		controllers.getProfile(req.user.id).then(result => res.send(result));
	});

//Routes - Get All User
	route.get('/all', (req, res) => {
		console.log('alluserrouter')
		controllers.getAllUser(req).then(result => res.send (result));
	});

//Route - Promote To Admin
	route.put('/:userId/enable', verify, verifyAdmin, (req, res) => {
		controllers.enableAdmin(req.params.productId).then(result => res.send(result));
	});


//Route - Demote To User
	route.put('/:userId/enable', verify, verifyAdmin, (req, res) => {
		controllers.disableAdmin(req.params.productId).then(result => res.send(result));
	});
 

//Route - Get Order History
	route.get('/history', auth.verify, controllers.history);

//##
//Get all orders (admin)
	route.get("/allOrders", verify, verifyAdmin, (req,res) => {
	controllers.getAllOrders().then(result => res.send(result))
})


//##
//Cancel This Order
	route.delete("/cancelOrder/:orderId", verify, (req,res) => {
	controllers.cancelOrder(req.params.orderId).then(result => res.send(result))
})


//Expose Route System
	module.exports = route;

