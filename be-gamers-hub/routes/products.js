//***ROUTES - PRODUCT***
//Dependencies and Modules
	const express = require('express');
	const route = express.Router();
	const ProductController = require('../controllers/products');
	const auth = require('../auth');
	const controllers = require('../controllers/products')

//Destructure
	const {verify, verifyAdmin} = auth;

//Routes - Create A Product
	route.post('/create', verify, verifyAdmin, (req, res) => {
		ProductController.addProduct(req.body).then(result => res.send(result));
	});

//Routes - Get All Products
	route.get('/all', (req, res) => {
		ProductController.getAllProducts().then(result => res.send(result));
	});

//Routes - Retrieve All Active products
	route.get('/active', (req, res) => {
		ProductController.getAllActive().then(result => res.send(result));
	});

//Routes - Retrieve This Product
	route.get('/:productId', (req, res) => {
		console.log(req.params.productId)
		ProductController.getProduct(req.params.productId).then(result => res.send(result));
	});

//Routes - Update Product Info
	route.put('/:productId', verify, verifyAdmin, (req, res) => {
		ProductController.updateProduct(req.params.productId, req.body).then(result => res.send(result));
	});

//Routes - Archive A Product
	route.put('/:productId/archive', verify, verifyAdmin, (req, res) => {
		ProductController.archiveProduct(req.params.productId).then(result => res.send(result));
	})

//Routes - Activate A Product
	route.put('/:productId/activate', verify, verifyAdmin, (req, res) => {
		ProductController.activateProduct(req.params.productId).then(result => res.send(result));
	})



module.exports = route;
