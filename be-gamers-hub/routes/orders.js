//***ROUTES - ORDER***
//Dependencies and Modules
  const Order = require("../models/Order");
  const controllers = require('../controllers/orders');
  const express = require('express');
  const auth = require('../auth');

//Routing Component
  const route = express.Router();

//Verify
  const { verify, verifyAdmin } = auth;

//Create Order
  route.post('/order', auth.verify, controllers.order);

//Update Order
  route.put("/orders/:orderId", verify, verifyAdmin, (req, res) => {
    
  });

//Get User Order
  route.get("/userOrder", verify, (req, res) => {
    let userID = req.user.id;
    controllers.getUserOrders(req, userID).then(result => res.send(result));
  });

//Get All Order
  route.get("/all", verify, verifyAdmin, (req, res) => {
    controllers.getAllOrders(req).then(result => res.send(result));
  });

//Delete An Order
  route.delete("/orders/:orderId", verify, verifyAdmin, (req, res) => {
    
  });

module.exports = route;