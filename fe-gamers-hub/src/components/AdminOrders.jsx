//Imports
import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import EditOrder from './EditOrder';

//Function
export default function AdminOrders(props) {
	console.log(`***AdminOrders1***`);
	const { ordersData, fetchData } = props;

	const [ orders, setOrders ] = useState([])

	useEffect(() => {
		const ordersArr = ordersData.map(order => {
			return(
				<tr key={order._id}>
					<td>{order._id}</td>
					<td>{order.userId}</td>
					<td>{order.products}</td>
					<th>ORDER ID</th>
					<th>UserId</th>
					<th>Products</th>
				</tr>
				)
		})
		setOrders(ordersArr)
	}, [ordersData])

	console.log(`***AdminOrders2***`);
	return(
		<>
			<div className="text-center my-4">
				<h1>All Orders</h1>
			</div>
			
			<Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr>
						<th>ORDER ID</th>
						<th>UserId</th>
						<th>Products</th>
					</tr>
				</thead>

				<tbody>
					{ orders }
				</tbody>
			</Table>

		</>

		)
}
