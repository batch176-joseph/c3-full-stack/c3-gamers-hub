//Imports


//Function
/*import React from 'react'

const Checkout = () => {
	return (
		<div>
			
		</div>
	)
}

export default Checkout*/

///////////////////////////////TEST HISTORY PAGE\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

//Imports
import UserView from '../components/UserView';
import AdminOrders from '../components/AdminOrders';
import { useContext, useEffect, useState } from 'react';
import UserContext from '../UserContext';
import { Button } from 'react-bootstrap'
import { Link } from 'react-router-dom';

//Function
export default function OrderPage() {

	const [ allOrders, setAllOrders ] = useState([])

	const fetchOrders = () => {
		// fetch('https://skygamershub.herokuapp.com/products/all')
		fetch('http://localhost:8000/orders/all')
		.then(res => res.json())
		.then(orders => {
			console.log(orders)
			setAllOrders(orders)
		})
	}

	useEffect(() => {
		fetchOrders()
	}, [])

	const { user } = useContext(UserContext);

	return(
		<>
			<h1>Orders</h1>
			{(user.isAdmin === true)?
			<AdminOrders ordersData={allOrders} fetchData={fetchOrders}/>
			:
			<UserView ordersData={allOrders}/>
			}
		</>
	)
}

